package com.learn.kafka.demo.components;

import com.learn.kafka.demo.constants.AppConstants;
import com.learn.kafka.demo.dto.LeadPublisherReqDto;
import com.learn.kafka.demo.dto.LeadPublisherRespDto;
import com.learn.kafka.demo.util.AppUtilService;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.clients.producer.RecordMetadata;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.support.SendResult;
import org.springframework.util.concurrent.ListenableFuture;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class LeadPublisherComponentServiceTest {

    @InjectMocks
    private LeadPublisherComponentService leadPublisherService;

    @Mock
    private KafkaTemplate<String, String> kafkaTemplate;

    @Mock
    private AppUtilService appUtilService;

    @BeforeEach
    public void setUp() {
        when(appUtilService.convertToJson(any(LeadPublisherReqDto.class))).thenReturn("Mocked JSON");
    }

    @Test
    void testPublishMessage_EmailAndMobile_Success() {
        LeadPublisherReqDto reqDto = new LeadPublisherReqDto();
        reqDto.setLeadEmailId("email");
        reqDto.setLeadMobileNumber("1234567890");

        ListenableFuture<SendResult<ProducerRecord<String, String>, RecordMetadata>> future =
                mock(ListenableFuture.class);
        when(kafkaTemplate.send(eq(AppConstants.TOPIC_NAME_EMAIL), any(String.class))).thenAnswer(invocation -> future);
        when(kafkaTemplate.send(eq(AppConstants.TOPIC_NAME_PHONE), any(String.class))).thenAnswer(invocation -> future);
        when(appUtilService.convertToJson(reqDto)).thenReturn("Mocked JSON");

        LeadPublisherRespDto result = leadPublisherService.publishMessage(reqDto);

        assertEquals(AppConstants.STATUS_PUBLISHED, result.getCode());
        assertEquals(AppConstants.SUCCESS_MSG, result.getMessage());
        verify(kafkaTemplate, times(1)).send(eq(AppConstants.TOPIC_NAME_EMAIL), any(String.class));
        verify(kafkaTemplate, times(1)).send(eq(AppConstants.TOPIC_NAME_PHONE), any(String.class));
    }

    @Test
    void testPublishMessage_EmailOnly_Success() {
        LeadPublisherReqDto reqDto = new LeadPublisherReqDto();
        reqDto.setLeadEmailId("email");

        ListenableFuture<SendResult<ProducerRecord<String, String>, RecordMetadata>> future =
                mock(ListenableFuture.class);
        when(kafkaTemplate.send(eq(AppConstants.TOPIC_NAME_EMAIL), any(String.class))).thenAnswer(invocation -> future);
        when(appUtilService.convertToJson(reqDto)).thenReturn("Mocked JSON");

        LeadPublisherRespDto result = leadPublisherService.publishMessage(reqDto);

        assertEquals(AppConstants.STATUS_PUBLISHED, result.getCode());
        assertEquals(AppConstants.SUCCESS_MSG, result.getMessage());
        verify(kafkaTemplate, times(1)).send(eq(AppConstants.TOPIC_NAME_EMAIL), any(String.class));
    }

    @Test
    void testPublishMessage_PhoneOnly_Success() {
        LeadPublisherReqDto reqDto = new LeadPublisherReqDto();
        reqDto.setLeadMobileNumber("1234567890");

        ListenableFuture<SendResult<ProducerRecord<String, String>, RecordMetadata>> future =
                mock(ListenableFuture.class);
        when(kafkaTemplate.send(eq(AppConstants.TOPIC_NAME_PHONE), any(String.class))).thenAnswer(invocation -> future);
        when(appUtilService.convertToJson(reqDto)).thenReturn("Mocked JSON");

        LeadPublisherRespDto result = leadPublisherService.publishMessage(reqDto);

        assertEquals(AppConstants.STATUS_PUBLISHED, result.getCode());
        assertEquals(AppConstants.SUCCESS_MSG, result.getMessage());
        verify(kafkaTemplate, times(1)).send(eq(AppConstants.TOPIC_NAME_PHONE), any(String.class));
    }

    @Test
    void testPublishMessage_Failure() {
        LeadPublisherReqDto reqDto = new LeadPublisherReqDto();
        reqDto.setLeadMobileNumber("1234567890");

        ListenableFuture<SendResult<ProducerRecord<String, String>, RecordMetadata>> future =
                mock(ListenableFuture.class);
        when(kafkaTemplate.send(eq(AppConstants.TOPIC_NAME_PHONE), any(String.class)))
                .thenAnswer(invocation -> new RuntimeException());
        when(appUtilService.convertToJson(reqDto)).thenReturn("Mocked JSON");

        LeadPublisherRespDto result = leadPublisherService.publishMessage(reqDto);

        assertEquals(AppConstants.STATUS_PUBLISHED_FAILED, result.getCode());
        assertEquals(AppConstants.FAILED_MSG, result.getMessage());
        verify(kafkaTemplate, times(1)).send(eq(AppConstants.TOPIC_NAME_PHONE), any(String.class));
    }
}