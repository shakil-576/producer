package com.learn.kafka.demo.util;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.learn.kafka.demo.dto.LeadPublisherReqDto;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;


@ExtendWith(MockitoExtension.class)
class AppUtilServiceTest {

    @Mock
    private ObjectMapper objectMapper;

    @InjectMocks
    private AppUtilService appUtilService;

    @Test
    public void testConvertToJson_Success() throws JsonProcessingException {
        LeadPublisherReqDto input = new LeadPublisherReqDto();
        input.setLeadName("NAME_TEST");
        String expectedJson = "{\"leadName\":\"NAME_TEST\"}";

        Mockito.when(objectMapper.writeValueAsString(input)).thenReturn(expectedJson);

        String actualJson = appUtilService.convertToJson(input);

        assertEquals(expectedJson, actualJson);
        verify(objectMapper, times(1)).writeValueAsString(input);
    }

    @Test
    public void testConvertToJson_Exception() throws JsonProcessingException {
        Object input = new Object();

        Mockito.when(objectMapper.writeValueAsString(input)).thenThrow(new JsonProcessingException("Mock Exception") {});

        String resultJson = appUtilService.convertToJson(input);

        assertNull(resultJson);
    }
}