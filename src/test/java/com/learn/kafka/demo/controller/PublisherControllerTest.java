package com.learn.kafka.demo.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.learn.kafka.demo.components.LeadPublisherComponentService;
import com.learn.kafka.demo.constants.AppConstants;
import com.learn.kafka.demo.dto.LeadPublisherReqDto;
import com.learn.kafka.demo.dto.LeadPublisherRespDto;
import com.learn.kafka.demo.util.AppUtilService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.math.BigDecimal;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@WebMvcTest(PublisherController.class)
class PublisherControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private AppUtilService appUtilService;

    @MockBean
    private LeadPublisherComponentService publisherComponentService;

    @InjectMocks
    private PublisherController publisherController;

    @Autowired
    private ObjectMapper objectMapper;

    @BeforeEach
    public void setUp() {
        MockitoAnnotations.openMocks(this);
        mockMvc = MockMvcBuilders.standaloneSetup(publisherController)
                                 .build();
    }

    @Test
    void test_successFullRequest() throws Exception {
        LeadPublisherReqDto reqDto = new LeadPublisherReqDto();
        reqDto.setLeadId(new BigDecimal(123));
        reqDto.setLeadEmailId("abc@g.com");
        reqDto.setPreferredMobileCommunicationMode(AppConstants.SMS);
        reqDto.setLeadMessage("I'm interested in chocolate flavoured cake of 3kg.");
        when(appUtilService.convertToJson(any())).thenReturn("<Mock Json>");

        LeadPublisherRespDto respDto = new LeadPublisherRespDto();
        respDto.setLeadId(new BigDecimal(123));
        respDto.setCode(AppConstants.STATUS_PUBLISHED);
        respDto.setMessage("Message has been published successfully.");

        when(publisherComponentService.publishMessage(any())).thenReturn(respDto);

        mockMvc.perform(MockMvcRequestBuilders.post("/lead/publish")
                                              .contentType(MediaType.APPLICATION_JSON)
                                              .content(objectMapper.writeValueAsString(reqDto)))
               .andExpect(MockMvcResultMatchers.status().isOk())
               .andExpect(MockMvcResultMatchers.jsonPath("$.code").value(AppConstants.STATUS_PUBLISHED))
               .andExpect(MockMvcResultMatchers.jsonPath("$.lead_id").value(123))
               .andExpect(MockMvcResultMatchers.jsonPath("$.message")
                                               .value("Message has been published successfully."));

    }

    @Test
    void test_badRequest() throws Exception {
        LeadPublisherReqDto reqDto = new LeadPublisherReqDto();
        when(appUtilService.convertToJson(any())).thenReturn("<Mock Json>");

        mockMvc.perform(MockMvcRequestBuilders.post("/lead/publish")
                                              .contentType(MediaType.APPLICATION_JSON)
                                              .content(objectMapper.writeValueAsString(reqDto)))
               .andExpect(MockMvcResultMatchers.status().isBadRequest());
    }

    @Test
    void test_methodNotAllowed() throws Exception {
        LeadPublisherReqDto reqDto = new LeadPublisherReqDto();
        when(appUtilService.convertToJson(any())).thenReturn("<JSON>");

        mockMvc.perform(MockMvcRequestBuilders.get("/lead/publish")
                                              .contentType(MediaType.APPLICATION_JSON)
                                              .content(objectMapper.writeValueAsString(reqDto)))
               .andExpect(MockMvcResultMatchers.status().isMethodNotAllowed());
    }

    @Test
    void test_unSupportedMediaType() throws Exception {
        LeadPublisherReqDto reqDto = new LeadPublisherReqDto();
        when(appUtilService.convertToJson(any())).thenReturn("<Mock Json>");

        mockMvc.perform(MockMvcRequestBuilders.get("/lead/publish")
                                              .contentType(MediaType.APPLICATION_PDF)
                                              .content(objectMapper.writeValueAsString(reqDto)))
               .andExpect(MockMvcResultMatchers.status().isMethodNotAllowed());
    }

    @Test
    void test_requestBodyMissing() throws Exception {
        LeadPublisherReqDto reqDto = new LeadPublisherReqDto();
        when(appUtilService.convertToJson(any())).thenReturn("<Mock Json>");

        mockMvc.perform(MockMvcRequestBuilders.post("/lead/publish")
                                              .contentType(MediaType.APPLICATION_JSON))
               .andExpect(MockMvcResultMatchers.status().isBadRequest());
    }
}
