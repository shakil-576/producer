package com.learn.kafka.demo.components;

import com.learn.kafka.demo.constants.AppConstants;
import com.learn.kafka.demo.dto.LeadPublisherReqDto;
import com.learn.kafka.demo.dto.LeadPublisherRespDto;
import com.learn.kafka.demo.util.AppUtilService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import java.util.concurrent.TimeUnit;

@Component
@Slf4j
public class LeadPublisherComponentService {

    @Autowired
    private KafkaTemplate kafkaTemplate;

    @Autowired
    private AppUtilService appUtilService;

    public LeadPublisherRespDto publishMessage(LeadPublisherReqDto reqDto) {
        LeadPublisherRespDto leadPublisherRespDto = new LeadPublisherRespDto();
        leadPublisherRespDto.setLeadId(reqDto.getLeadId());
        leadPublisherRespDto.setCode(AppConstants.STATUS_PUBLISHED_FAILED);
        leadPublisherRespDto.setMessage(AppConstants.FAILED_MSG);

        if (!StringUtils.isEmpty(reqDto.getLeadEmailId()) && !StringUtils.isEmpty(reqDto.getLeadMobileNumber())) {
            if (publishStatus(AppConstants.TOPIC_NAME_EMAIL, reqDto) && publishStatus(AppConstants.TOPIC_NAME_PHONE,
                    reqDto)) {
                leadPublisherRespDto.setCode(AppConstants.STATUS_PUBLISHED);
                leadPublisherRespDto.setMessage(AppConstants.SUCCESS_MSG);
            }
        } else if (!StringUtils.isEmpty(reqDto.getLeadMobileNumber())) {
            if (publishStatus(AppConstants.TOPIC_NAME_PHONE, reqDto)) {
                leadPublisherRespDto.setCode(AppConstants.STATUS_PUBLISHED);
                leadPublisherRespDto.setMessage(AppConstants.SUCCESS_MSG);
            }
        } else if (!StringUtils.isEmpty(reqDto.getLeadEmailId())) {
            if (publishStatus(AppConstants.TOPIC_NAME_EMAIL, reqDto)) {
                leadPublisherRespDto.setCode(AppConstants.STATUS_PUBLISHED);
                leadPublisherRespDto.setMessage(AppConstants.SUCCESS_MSG);
            }
        }

        return leadPublisherRespDto;
    }

    private boolean publishStatus(String topicName, LeadPublisherReqDto reqDto) {
        boolean status = false;
        String message = appUtilService.convertToJson(reqDto);

        try {
            kafkaTemplate.send(topicName, message).get(15, TimeUnit.SECONDS);
            log.info("topicName {}, LeadId {}, Published message successfully", topicName, reqDto.getLeadId());
            status = true;
        } catch (Exception e) {
            log.info("Failed to publish message=[" + message + "] due to : " + e.getMessage());
        }

        return status;
    }
}
