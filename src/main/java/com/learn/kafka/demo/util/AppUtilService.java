package com.learn.kafka.demo.util;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class AppUtilService {

    @Autowired
    private ObjectMapper objectMapper;

    public String convertToJson(Object input) {
        String jsonString = null;
        try {
            jsonString = objectMapper.writeValueAsString(input);
        } catch (JsonProcessingException e) {
            log.error("Exception Msg {} ", e.getMessage());
        }
        return jsonString;
    }
}
