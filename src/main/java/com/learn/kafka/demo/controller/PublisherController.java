package com.learn.kafka.demo.controller;

import com.learn.kafka.demo.components.LeadPublisherComponentService;
import com.learn.kafka.demo.dto.LeadPublisherReqDto;
import com.learn.kafka.demo.dto.LeadPublisherRespDto;
import com.learn.kafka.demo.util.AppUtilService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@Validated
@Slf4j
public class PublisherController {

    @Autowired
    private AppUtilService appUtilService;

    @Autowired
    private LeadPublisherComponentService publisherComponentService;

    @PostMapping("/lead/publish")
    public ResponseEntity<LeadPublisherRespDto> yourEndpoint(
            @RequestBody @Valid LeadPublisherReqDto leadPublisherReqDto
    ) {
        log.info("leadPublisherReqDto {} ", appUtilService.convertToJson(leadPublisherReqDto));

        LeadPublisherRespDto leadPublisherRespDto = publisherComponentService.publishMessage(leadPublisherReqDto);

        log.info("leadPublisherRespDto {} ", appUtilService.convertToJson(leadPublisherRespDto));

        return new ResponseEntity<>(leadPublisherRespDto, HttpStatus.OK);
    }
}
