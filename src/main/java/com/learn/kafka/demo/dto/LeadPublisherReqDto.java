package com.learn.kafka.demo.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.learn.kafka.demo.validation.ValidLeadInfo;
import com.learn.kafka.demo.validation.ValidPreferredMobileCommunicationMode;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.validation.annotation.Validated;

import javax.validation.constraints.Digits;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Positive;
import javax.validation.constraints.Size;
import java.math.BigDecimal;

@Data
@NoArgsConstructor
@Validated
@ValidLeadInfo
public class LeadPublisherReqDto {

    @NotNull(message = "leadId is mandatory")
    @Digits(integer = 10, fraction = 0, message = "leadId must be a positive number without decimals")
//    @Positive(message = "leadId must be a positive number without decimals")
    @JsonProperty("lead_id")
    private BigDecimal leadId;

    private String source;

    @JsonProperty("lead_name")
    private String leadName;

    @Pattern(regexp = "^(?:\\+\\d{1,3}[- ]?)?\\d{10,12}$", message = "Invalid lead mobile number format")
    @JsonProperty("lead_mobile_number")
    private String leadMobileNumber;

    @Email(message = "Invalid email format")
    @JsonProperty("lead_email_id")
    private String leadEmailId;

    @ValidPreferredMobileCommunicationMode
    @JsonProperty("preferred_mobile_communication_mode")
    private String preferredMobileCommunicationMode;

    @NotBlank(message = "leadMessage field is mandatory")
    @Size(min = 10, message = "leadMessage field must have at least 10 characters")
    @JsonProperty("lead_message")
    private String leadMessage;
}
