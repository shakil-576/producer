package com.learn.kafka.demo.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

@Data
@NoArgsConstructor
public class LeadPublisherRespDto {

    @JsonProperty("lead_id")
    private BigDecimal leadId;

    private String code;

    private String message;
}
