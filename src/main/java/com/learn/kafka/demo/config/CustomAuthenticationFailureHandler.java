package com.learn.kafka.demo.config;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.learn.kafka.demo.dto.LeadPublisherRespDto;
import com.learn.kafka.demo.util.AppUtilService;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class CustomAuthenticationFailureHandler implements AuthenticationFailureHandler {

    private ObjectMapper objectMapper = new ObjectMapper();

    @Override
    public void onAuthenticationFailure(HttpServletRequest request, HttpServletResponse response,
                                        AuthenticationException exception) throws IOException, ServletException {

        LeadPublisherRespDto respDto = new LeadPublisherRespDto();
        respDto.setCode(String.valueOf(HttpStatus.UNAUTHORIZED.value()));
        respDto.setMessage("Access Unauthorised");

        response.getOutputStream()
                .println(objectMapper.writeValueAsString(respDto));

    }
}
