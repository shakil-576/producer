package com.learn.kafka.demo.config;

import com.learn.kafka.demo.dto.LeadPublisherRespDto;
import com.learn.kafka.demo.util.AppUtilService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.support.DefaultMessageSourceResolvable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.util.CollectionUtils;
import org.springframework.validation.FieldError;
import org.springframework.web.HttpMediaTypeNotSupportedException;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.util.List;
import java.util.stream.Collectors;

@RestControllerAdvice
public class GlobalExceptionHandler {

    @Autowired
    private AppUtilService appUtilService;

    @ExceptionHandler(HttpRequestMethodNotSupportedException.class)
    public ResponseEntity<LeadPublisherRespDto> requestMethodNotSupportedException(
            HttpRequestMethodNotSupportedException ex) {
        LeadPublisherRespDto leadPublisherRespDto = new LeadPublisherRespDto();
        leadPublisherRespDto.setCode(String.valueOf(HttpStatus.METHOD_NOT_ALLOWED.value()));
        leadPublisherRespDto.setMessage(ex.getMessage());

        return new ResponseEntity<>(leadPublisherRespDto, HttpStatus.OK);
    }

    @ExceptionHandler(HttpMessageNotReadableException.class)
    public ResponseEntity<LeadPublisherRespDto> httpMessageNotReadableException(HttpMessageNotReadableException ex) {
        LeadPublisherRespDto leadPublisherRespDto = new LeadPublisherRespDto();
        leadPublisherRespDto.setCode(String.valueOf(HttpStatus.BAD_REQUEST.value()));
        leadPublisherRespDto.setMessage("Request body is missing");

        return new ResponseEntity<>(leadPublisherRespDto, HttpStatus.OK);
    }

    @ExceptionHandler(HttpMediaTypeNotSupportedException.class)
    public ResponseEntity<LeadPublisherRespDto> handleUnSupportedMediaType(HttpMediaTypeNotSupportedException ex) {
        LeadPublisherRespDto leadPublisherRespDto = new LeadPublisherRespDto();
        leadPublisherRespDto.setCode(String.valueOf(HttpStatus.UNSUPPORTED_MEDIA_TYPE.value()));
        leadPublisherRespDto.setMessage(ex.getMessage());

        return new ResponseEntity<>(leadPublisherRespDto, HttpStatus.OK);
    }

    @ExceptionHandler(MethodArgumentNotValidException.class)
    public ResponseEntity<LeadPublisherRespDto> handleValidationErrors(MethodArgumentNotValidException ex) {
        List<String> errors = null;
        LeadPublisherRespDto leadPublisherRespDto = new LeadPublisherRespDto();

        errors = ex.getBindingResult().getFieldErrors()
                   .stream().map(FieldError::getDefaultMessage).collect(Collectors.toList());
        leadPublisherRespDto.setCode(String.valueOf(HttpStatus.BAD_REQUEST.value()));

        if (!CollectionUtils.isEmpty(errors)) {
            leadPublisherRespDto.setMessage(appUtilService.convertToJson(errors));
        } else {
            errors = ex.getBindingResult().getAllErrors().stream()
                       .map(DefaultMessageSourceResolvable::getDefaultMessage).collect(Collectors.toList());
            if (!CollectionUtils.isEmpty(errors)) {
                leadPublisherRespDto.setMessage(appUtilService.convertToJson(errors));
            }
        }

        return new ResponseEntity<>(leadPublisherRespDto, HttpStatus.OK);
    }
}
