package com.learn.kafka.demo.constants;

public class AppConstants {
    public static final String SMS = "SMS";
    public static final String WHATSAPP = "WHATSAPP";
    public static final String PUSH_NOTIFICATION = "PUSH_NOTIFICATION";
    public static final String TOPIC_NAME_EMAIL = "EMAIL";
    public static final String TOPIC_NAME_PHONE = "PHONE";
    public static final String STATUS_PUBLISHED = "PUBLISHED";
    public static final String STATUS_PUBLISHED_FAILED = "PUBLISHED_FAILED";
    public static final String SUCCESS_MSG = "Message has been published successfully.";
    public static final String FAILED_MSG = "Failed to publish message. Please try again later";
}
