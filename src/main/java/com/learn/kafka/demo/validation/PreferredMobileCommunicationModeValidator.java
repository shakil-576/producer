package com.learn.kafka.demo.validation;

import com.learn.kafka.demo.constants.AppConstants;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class PreferredMobileCommunicationModeValidator implements ConstraintValidator<ValidPreferredMobileCommunicationMode, String> {

    @Override
    public void initialize(ValidPreferredMobileCommunicationMode constraintAnnotation) {
    }

    @Override
    public boolean isValid(String value, ConstraintValidatorContext context) {
        return AppConstants.SMS.equals(value) || AppConstants.WHATSAPP.equals(value) || AppConstants.PUSH_NOTIFICATION
                .equals(value);
    }
}
