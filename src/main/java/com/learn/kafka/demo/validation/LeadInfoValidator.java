package com.learn.kafka.demo.validation;

import com.learn.kafka.demo.dto.LeadPublisherReqDto;
import org.springframework.util.StringUtils;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class LeadInfoValidator implements ConstraintValidator<ValidLeadInfo, LeadPublisherReqDto> {

    @Override
    public void initialize(ValidLeadInfo constraintAnnotation) {

    }

    @Override
    public boolean isValid(LeadPublisherReqDto value, ConstraintValidatorContext context) {
        return (!StringUtils.isEmpty(value.getLeadMobileNumber()) || !StringUtils.isEmpty(value.getLeadEmailId()));
    }
}
