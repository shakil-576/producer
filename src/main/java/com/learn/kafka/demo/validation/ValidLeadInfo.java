package com.learn.kafka.demo.validation;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = {LeadInfoValidator.class})
@Documented
public @interface ValidLeadInfo {
    String message() default "Either lead_mobile_number or lead_email_id is mandatory";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
