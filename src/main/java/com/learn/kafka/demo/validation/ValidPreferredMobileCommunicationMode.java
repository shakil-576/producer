package com.learn.kafka.demo.validation;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target({ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = PreferredMobileCommunicationModeValidator.class)
@Documented
public @interface ValidPreferredMobileCommunicationMode {
    String message() default "Invalid preferred mobile communication mode";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
